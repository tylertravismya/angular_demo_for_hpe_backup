import { BaseService } from '../../../services/Base.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Base } from '../../../models/Base';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexBaseComponent implements OnInit {

  bases: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: BaseService) {}

  ngOnInit() {
    this.getBases();
  }

  getBases() {
    this.service.getBases().subscribe(res => {
      this.bases = res;
    });
  }

  deleteBase(id) {
    this.service.deleteBase(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexBase']));
			});  }
}
