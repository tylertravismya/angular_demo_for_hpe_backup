import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { BaseService } from '../../../services/Base.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Base/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateBaseComponent extends SubBaseComponent implements OnInit {

  title = 'Add Base';
  baseForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private baseservice: BaseService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.baseForm = this.fb.group({
   });
  }
  addBase() {
      this.baseservice.addBase()
      	.then(success => this.router.navigate(['/indexBase']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
