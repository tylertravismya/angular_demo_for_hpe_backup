var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Player
var Player = new Schema({
  playerId: {
	type : String
  },
  name: {
	type : String
  },
  dateOfBirth: {
	type : String
  },
  height: {
	type : String
  },
  isProfessional: {
	type : Boolean
  },
},{
    collection: 'players'
});

module.exports = mongoose.model('Player', Player);