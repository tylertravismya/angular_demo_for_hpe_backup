var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Game
var Game = new Schema({
  gameId: {
	type : String
  },
  frames: {
	type : Number
  },
  Player: {
	type : Schema.Types.ObjectId
  },
},{
    collection: 'games'
});

module.exports = mongoose.model('Game', Game);