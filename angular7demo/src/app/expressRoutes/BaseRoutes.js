// baseRoutes.js

var express = require('express');
var app = express();
var baseRoutes = express.Router();

// Require Item model in our routes module
var Base = require('../models/Base');

// Defined store route
baseRoutes.route('/add').post(function (req, res) {
	var base = new Base(req.body);
	base.save()
    .then(item => {
    	res.status(200).json({'base': 'Base added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
baseRoutes.route('/').get(function (req, res) {
	Base.find(function (err, bases){
		if(err){
			console.log(err);
		}
		else {
			res.json(bases);
		}
	});
});

// Defined edit route
baseRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	Base.findById(id, function (err, base){
		res.json(base);
	});
});

//  Defined update route
baseRoutes.route('/update/:id').post(function (req, res) {
	Base.findById(req.params.id, function(err, base) {
		if (!base)
			return next(new Error('Could not load a Base Document using id ' + req.params.id));
		else {

			base.save().then(base => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
baseRoutes.route('/delete/:id').get(function (req, res) {
   Base.findOneAndDelete({_id: req.params.id}, function(err, base){
        if(err) res.json(err);
        else res.json('Successfully removed ' + Base + ' using id ' + req.params.id );
    });
});

module.exports = baseRoutes;